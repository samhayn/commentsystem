<?php
// Init
date_default_timezone_set("UTC");
define('PROJECT_ROOT', dirname(__DIR__));
require PROJECT_ROOT . '/vendor/autoload.php';

$config = parse_ini_file(PROJECT_ROOT . '/config/config.ini');

$app = new \Slim\Slim([
    'log.enabled' => false,
]);

$app->response()->header('Content-Type', 'application/json');

$app->post('/posts', function () use ($app, $config) {
    (new Controller($app, $config))->createPost();
});

$app->get('/posts', function () use ($app, $config) {
    (new Controller($app, $config))->getPosts();
});

$app->post('/posts/comment', function () use ($app, $config) {
    (new Controller($app, $config))->createComment();
});
$app->get('/posts/:id/comment', function ($id) use ($app, $config) {
    (new Controller($app, $config))->getComments($id);
});

$app->run();
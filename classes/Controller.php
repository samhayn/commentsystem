<?php

class Controller
{
    /**
     * @var  \Slim\Slim
     */
    private $slim = null;

    /**
     * @var array
     */
    private $config = [];

    function __construct(\Slim\Slim $slim, array $config)
    {
        $this->setSlim($slim)->setConfig($config);
    }

    /**
     * @return \Slim\Slim
     */
    public function getSlim()
    {
        return $this->slim;
    }

    /**
     * @param \Slim\Slim $slim
     * @return \Controller
     */
    public function setSlim(\Slim\Slim $slim)
    {
        $this->slim = $slim;
        return $this;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param array $config
     * @return \Controller
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * Creates post for blog, result is returned as status code
     */
    public function createPost()
    {
        $encodedBbody = $this->getSlim()->request()->getBody();
        echo $encodedBbody;
        $body = json_decode($encodedBbody,true);

        if (!isset($body[Models\Post::FIELD_EMAIL]) ||
            !isset($body[Models\Post::FIELD_MESSAGE]) ||
            !isset($body[Models\Post::FIELD_NAME])
        )
        {
            $this->getSlim()->response()->setStatus(400);
            $this->getSlim()->response()->setBody('Bad request');
            return;
        }
        $postMapper = new Mappers\PostMapper($this->connect());

        $post = new Models\Post($body);
        if ($postMapper->create($post) === false)
        {
            $this->getSlim()->response()->setStatus(500);
            $this->getSlim()->response()->setBody('Server error');
            return;
        }

        $this->getSlim()->response()->setStatus(201);
        $this->getSlim()->response()->setBody('Created');
    }

    /**
     * Creates comment for post of blog, result is returned as status code
     */
    public function createComment()
    {
        $encodedBbody = $this->getSlim()->request()->getBody();
        $body = json_decode($encodedBbody,true);

        if (!isset($body[Models\Comment::FIELD_EMAIL]) ||
            !isset($body[Models\Comment::FIELD_MESSAGE]) ||
            !isset($body[Models\Comment::FIELD_NAME]) ||
            !isset($body[Models\Comment::FIELD_POST_ID])
        )
        {
            $this->getSlim()->response()->setStatus(400);
            $this->getSlim()->response()->setBody('Bad request');
            return;
        }

        $commentMapper = new Mappers\CommentMapper($this->connect());

        $comment = new Models\Comment($body);

        try {
            if ($commentMapper->create($comment) === false)
            {
                $this->getSlim()->response()->setStatus(500);
                $this->getSlim()->response()->setBody('Server error');
                return;
            }
        }
        catch ( Models\Exceptions\AppException $e)
        {
            $this->getSlim()->response()->setStatus($e->getCode());
            $this->getSlim()->response()->setBody($e->getMessage());
            return;
        }

        $this->getSlim()->response()->setStatus(201);
        $this->getSlim()->response()->setBody('Created');
    }

    /**
     * Returns posts and their number as a JSON
     */
    public function getPosts()
    {
        $postMapper = new Mappers\PostMapper($this->connect());
        $posts = $postMapper->getPosts();
        $response = [
            'count' => count($posts),
            'posts' => $posts
        ];

        $this->getSlim()->response()->setStatus(200);
        $this->getSlim()->response()->setBody(json_encode($response));
    }

    /**
     * Returns comments for certain post id and their number as a JSON
     */
    public function getComments($postId)
    {
        $commentMapper = new Mappers\CommentMapper($this->connect());
        $comments = $commentMapper->getComments($postId);
        $response = [
            'count' => count($comments),
            'comments' => $comments
        ];

        $this->getSlim()->response()->setStatus(200);
        $this->getSlim()->response()->setBody(json_encode($response));
    }

    /**
     * Creates mysql connection
     * @return resource
     */
    private function connect()
    {
        $connection = mysql_connect(
            $this->getConfig()['host'],
            $this->getConfig()['user'],
            $this->getConfig()['passwd']
        );
        mysql_select_db($this->getConfig()['name'], $connection);

        return $connection;
    }
}

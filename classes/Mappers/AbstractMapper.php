<?php
namespace Mappers;

class AbstractMapper
{
    /**
     * @var resource
     */
    protected $resource = null;

    function __construct($resource)
    {
        $this->setResource($resource);
    }

    /**
     * @return resource
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @param resource $resource
     * @return \Mappers\AbstractMapper
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
        return $this;
    }

    /**
     * Alias of mysql_query
     *
     * @param string $query
     * @return resource
     */
    protected function query($query)
    {
        return mysql_query($query, $this->getResource());
    }

    /**
     * Alias of mysql_query with mysql_fetch_assoc
     *
     * @param string $query
     * @return array
     */
    protected function read($query)
    {
        $result = $this->query($query);

        $data = [];

        while ($row = mysql_fetch_assoc($result))
        {
            $data[] = $row;
        }

        return $data;
    }

    /**
     * Escapes string using htmlentities and mysql native function
     *
     * @param string $string
     * @return string
     */
    protected function escape($string)
    {
        return mysql_real_escape_string(htmlentities($string));
    }
}

<?php
namespace Mappers;

class CommentMapper extends AbstractMapper
{
    /**
     * Inserts comment into DB if posts exists
     * @param \Models\Comment $comment
     * @return int - inserted id
     * @throws \Models\Exceptions\AppException
     */
    public function create(\Models\Comment $comment)
    {
        $this->checkPostExists($comment->getPostId());
        return $this->insertPost($comment);
    }

    /**
     * Returns comments for certain post id
     *
     * @param int $postId
     * @return array
     */
    public function getComments($postId)
    {
        return $this->read(
            'SELECT `email`,`message`,`name` FROM `post` WHERE  `post_id` = ' .
            $this->escape($postId)
        );
    }

    /**
     * Check is post with certain id exists
     *
     * @param int $postId
     * @throws \Models\Exceptions\AppException
     */
    private function checkPostExists($postId)
    {
        $read = $this->read(
            'SELECT `id` FROM `post` WHERE `id` = ' .
            $this->escape($postId)
        );

        if (!$read)
        {
            throw new \Models\Exceptions\AppException(
                'Post not found', 404
            );
        }
    }

    /**
     * @param \Models\Comment $comment
     * @return int - inserted id
     */
    private function insertPost(\Models\Comment $comment)
    {
        $query = "INSERT INTO `post`(`name`,`email`,`message`,`post_id`) VALUES('%s','%s','%s', %d)";
        $this->query(
            sprintf($query,
                $this->escape($comment->getName()),
                $this->escape($comment->getEmail()),
                $this->escape($comment->getMessage()),
                $this->escape($comment->getPostId())
            )
        );

        return mysql_insert_id($this->getResource());
    }
}

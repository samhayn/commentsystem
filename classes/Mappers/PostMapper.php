<?php
namespace Mappers;
class PostMapper extends AbstractMapper
{
    /**
     * Creates new post
     *
     * @param \Models\Post $post
     * @return int - inserted id
     */
    public function create(\Models\Post $post)
    {
        $query = "INSERT INTO `post`(`name`,`email`,`message`) VALUES('%s','%s','%s')";
        $this->query(
            sprintf($query,
                $this->escape($post->getName()),
                $this->escape($post->getEmail()),
                $this->escape($post->getMessage())
            )
        );
        return mysql_insert_id($this->getResource());
    }

    /**
     * Returns all posts
     *
     * @return array
     */
    public function getPosts()
    {
        return $this->read('SELECT `id`,`name`,`email`,`message` FROM `post` WHERE ISNULL(`post_id`)');
    }
}

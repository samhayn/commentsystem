<?php
namespace Models;

class Comment extends Post
{
    const FIELD_POST_ID = 'post_id';

    /**
     * @var int
     */
    protected $postId = 0;

    public function __construct(array $fields)
    {
        parent::__construct($fields);

        if (!isset($fields[self::FIELD_POST_ID]) || empty($fields[self::FIELD_POST_ID]))
        {
            throw new Exceptions\AppException('Bad request', 400);
        }
        $this->setPostId($fields[self::FIELD_POST_ID]);
    }

    /**
     * @return int
     */
    public function getPostId()
    {
        return $this->postId;
    }

    /**
     * @param int $postId
     * @return \Models\Comment
     */
    public function setPostId($postId)
    {
        $this->postId = $postId;
        return $this;
    }
}

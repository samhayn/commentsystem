<?php

namespace Models;

class Post
{
    const FIELD_EMAIL = 'email';
    const FIELD_NAME = 'name';
    const FIELD_MESSAGE = 'message';

    /**
     * @var string
     */
    protected $email = '';

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $message = '';

    public function __construct(array $fields)
    {
        if (!isset($fields[self::FIELD_EMAIL])
            || !isset($fields[self::FIELD_NAME])
            || !isset($fields[self::FIELD_MESSAGE])
        )
        {
            throw new Exceptions\AppException('Bad request', 400);
        }

        $this->setEmail($fields[self::FIELD_EMAIL])
            ->setName($fields[self::FIELD_NAME])
            ->setMessage($fields[self::FIELD_MESSAGE]);
    }

    /**
     * Post's email
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Post's name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Post's message
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param type string
     * @return \Models\Post
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param type string
     * @return \Models\Post
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param type string
     * @return \Models\Post
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }
}

SET NAMES UTF8;
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
    `id` INT( 11 ) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK. It is a unique ID of the post',
    `name` VARCHAR( 255 ) NOT NULL COMMENT 'Author name',
    `email` VARCHAR( 255 ) NOT NULL COMMENT 'Author email',
    `message` MEDIUMTEXT NOT NULL COMMENT 'Unique language identifier',
    `post_id` VARCHAR( 5000 ) NULL COMMENT 'Id of post, comment related to, it is identifier of comment also',
    PRIMARY KEY (`id`),
    KEY `post_id` (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='This table contains posts and comments';
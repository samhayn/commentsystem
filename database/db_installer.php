<?php
define('PROJECT_ROOT', dirname(__DIR__));
require_once PROJECT_ROOT . '/vendor/autoload.php';
$config = parse_ini_file(PROJECT_ROOT . '/config/config.ini');

$command = 'mysql -u' . $config['user'] . ' -p' . $config['passwd'] .
    ' -h' . $config['host'] . ' ' . $config['name'] . '<' .
    PROJECT_ROOT . '/database/db.sql';

exec($command, $out, $result);

if ($result)
{
    exit(1);
}

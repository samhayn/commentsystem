<?php

function analizeRecursive($dir) {
   $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
      (is_dir("$dir/$file")) ? analizeRecursive("$dir/$file") : executeAnalysis("$dir/$file");
    }
}

function executeAnalysis($file)
{
    exec('php -l ' . $file, $output, $return);

    echo $output[0] . "\n";

    if ($return)
    {
        exit(1);
    }
}

analizeRecursive('api');
analizeRecursive('classes');

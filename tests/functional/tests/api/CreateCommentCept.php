<?php
$I = new ApiGuy($scenario);
$I->wantTo('create new comment for post');
$postBody = [
    'name' => 'Test user',
    'email' => 'Test email',
    'message' => 'Test post message'
];
$I->sendPOST('/posts',  json_encode($postBody));

$I->seeResponseCodeIs(201);
$I->seeResponseContains('Created');

$commentBody = [
    'name' => 'Test user',
    'email' => 'Test email',
    'message' => 'Test post message',
    'post_id' => 1,
];
$I->sendPOST('/posts/comment',  json_encode($commentBody));
$I->seeResponseCodeIs(201);
$I->seeResponseContains('Created');
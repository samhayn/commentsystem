<?php
$I = new ApiGuy($scenario);
$I->wantTo('Test creation of comment withou post');
$body = [
    'name' => 'Test user',
    'email' => 'Test email',
    'message' => 'Test post message',
    'post_id' => 100,
];
$I->sendPOST('/posts/comment',  json_encode($body));

$I->seeResponseContains('Post not found');
$I->seeResponseCodeIs(404);

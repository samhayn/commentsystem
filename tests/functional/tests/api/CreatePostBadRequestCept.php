<?php
$I = new ApiGuy($scenario);
$I = new ApiGuy($scenario);
$I->wantTo('Test bad request on new post creation');
$body = [
    'email' => 'Test email',
    'message' => 'Test post message'
];
$I->sendPOST('/posts',  json_encode($body));

$I->seeResponseCodeIs(400);
$I->seeResponseContains('Bad request');

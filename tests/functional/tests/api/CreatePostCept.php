<?php
$I = new ApiGuy($scenario);
$I->wantTo('create new post');
$body = [
    'name' => 'Test user',
    'email' => 'Test email',
    'message' => 'Test post message'
];
$I->sendPOST('/posts',  json_encode($body));

$I->seeResponseCodeIs(201);
$I->seeResponseContains('Created');

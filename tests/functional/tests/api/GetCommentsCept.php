<?php
$I = new ApiGuy($scenario);
$I->wantTo('Get comments for post');

$body = [
    'name' => 'Test user',
    'email' => 'Test email',
    'message' => 'Test post message'
];
$I->sendPOST('/posts',  json_encode($body));

$I->seeResponseCodeIs(201);

$commentBody = [
    'name' => 'Test user',
    'email' => 'Test email',
    'message' => 'Test post message',
    'post_id' => 1,
];
$I->sendPOST('/posts/comment',  json_encode($commentBody));
$I->seeResponseCodeIs(201);

$I->sendGET('/posts/1/comment');

$I->seeResponseCodeIs(200);

unset($commentBody['post_id']);
$I->seeResponseContainsJson(['count' => 1, 'comments' => $commentBody]);
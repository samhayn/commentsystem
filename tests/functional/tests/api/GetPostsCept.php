<?php
$I = new ApiGuy($scenario);
$I->wantTo('Get all posts');

$body = [
    'name' => 'Test user',
    'email' => 'Test email',
    'message' => 'Test post message'
];
$I->sendPOST('/posts',  json_encode($body));

$I->seeResponseCodeIs(201);

$I->sendGET('/posts');
$I->seeResponseIsJson();
$I->seeResponseCodeIs(200);
$I->seeResponseContainsJson(['count' => 1, 'posts' => [$body]]);
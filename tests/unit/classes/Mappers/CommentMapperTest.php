<?php
use Mappers\CommentMapper;
class CommentMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function construct()
    {
        $resource = mysql_connect('localhost','root','root');
        mysql_select_db('blog', $resource);

        $mapper = new CommentMapper($resource);
        $this->assertInstanceOf('\Mappers\CommentMapper', $mapper);
        $this->assertInstanceOf('\Mappers\AbstractMapper', $mapper);
    }

    /**
     * @test
     * @expectedException \Models\Exceptions\AppException
     * @expectedExceptionCode 404
     * @expectedExceptionMessage Post not found
     */
    public function createNotExistentPostId()
    {
        $resource = mysql_connect('localhost','root','root');
        mysql_select_db('blog', $resource);

        $mapper = new CommentMapper($resource);

        $fields = [
            Models\Comment::FIELD_EMAIL => 'test@email.com',
            Models\Comment::FIELD_MESSAGE => 'test message',
            Models\Comment::FIELD_NAME => 'test author',
            Models\Comment::FIELD_POST_ID => 1,
        ];
        $comment = new Models\Comment($fields);

        $mapper->create($comment);
    }

    /**
     * @test
     */
    public function create()
    {
        $resource = mysql_connect('localhost','root','root');
        mysql_select_db('blog', $resource);

        $commentMapper = new CommentMapper($resource);
        $postMapper = new Mappers\PostMapper($resource);

        $postFields = [
            Models\Post::FIELD_EMAIL => 'test@email.com',
            Models\Post::FIELD_MESSAGE => 'test message',
            Models\Post::FIELD_NAME => 'test author',
        ];
        $post = new Models\Post($postFields);

        $postId = $postMapper->create($post);
        $this->assertGreaterThan(0, $postId);

        $commentFields = [
            Models\Comment::FIELD_EMAIL => 'test@email.com',
            Models\Comment::FIELD_MESSAGE => 'test message',
            Models\Comment::FIELD_NAME => 'test author',
            Models\Comment::FIELD_POST_ID => $postId,
        ];
        $comment = new Models\Comment($commentFields);

        $this->assertGreaterThan(0, $commentMapper->create($comment));

        $result = mysql_query('SELECT `email`,`message`,`name` FROM `post` WHERE  `post_id` = ' . $postId);

        $data = [];

        while ($row = mysql_fetch_assoc($result))
        {
            $data[] = $row;
        }
        $this->assertEquals(1, count($data));
        unset($commentFields[\Models\Comment::FIELD_POST_ID]);
        $this->assertEquals($commentFields, $data[0]);

        return $resource;
    }

    /**
     * @test
     * @depends create
     */
    public function getComments($resource)
    {
        $commentMapper = new CommentMapper($resource);
        $postMapper = new Mappers\PostMapper($resource);
        $posts = $postMapper->getPosts();

        $comments = $commentMapper->getComments($posts[0]['id']);
        $this->assertEquals(1, count($comments));

        $result = mysql_query('SELECT `email`,`message`,`name` FROM `post` WHERE  `post_id` = ' . $posts[0]['id']);

        $data = [];

        while ($row = mysql_fetch_assoc($result))
        {
            $data[] = $row;
        }
        $this->assertEquals(1, count($data));
        $this->assertEquals($comments, $data);
    }

    public static function tearDownAfterClass()
    {
        parent::tearDownAfterClass();
        $connection = mysql_connect('localhost','root','root');
        mysql_select_db('blog', $connection);
        mysql_query('TRUNCATE `post`');
    }
}

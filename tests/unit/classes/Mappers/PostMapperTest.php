<?php
use Mappers\PostMapper;
class PostMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function construct()
    {
        $resource = mysql_connect('localhost','root','root');
        mysql_select_db('blog', $resource);

        $mapper = new PostMapper($resource);
        $this->assertInstanceOf('\Mappers\PostMapper', $mapper);
        $this->assertInstanceOf('\Mappers\AbstractMapper', $mapper);
    }

    /**
     * @test
     */
    public function create()
    {
        $resource = mysql_connect('localhost','root','root');
        mysql_select_db('blog', $resource);

        $mapper = new PostMapper($resource);

        $fields = [
            Models\Post::FIELD_EMAIL => 'test@email.com',
            Models\Post::FIELD_MESSAGE => 'test message',
            Models\Post::FIELD_NAME => 'test author',
        ];
        $post = new Models\Post($fields);

        $this->assertGreaterThan(0, $mapper->create($post));

        $result = mysql_query('SELECT `email`,`message`,`name` FROM `post` WHERE  ISNULL(`post_id`)');

        $data = [];

        while ($row = mysql_fetch_assoc($result))
        {
            $data[] = $row;
        }
        $this->assertEquals(1, count($data));

        return $resource;
    }

    /**
     * @test
     * @depends create
     */
    public function getPosts($resource)
    {
        $mapper = new PostMapper($resource);
        $posts = $mapper->getPosts();

        $this->assertEquals(1, count($posts));

        $result = mysql_query('SELECT `id`,`email`,`message`,`name` FROM `post` WHERE  ISNULL(`post_id`)');

        $data = [];

        while ($row = mysql_fetch_assoc($result))
        {
            $data[] = $row;
        }

        $this->assertEquals($data, $posts);
    }

    public static function tearDownAfterClass()
    {
        parent::tearDownAfterClass();
        $connection = mysql_connect('localhost','root','root');
        mysql_select_db('blog', $connection);
        mysql_query('TRUNCATE `post`');
    }
}

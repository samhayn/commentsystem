<?php
use Models\Comment;

class CommentTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function construct()
    {
        $fields = [
            Comment::FIELD_EMAIL => 'test@localhost.com',
            Comment::FIELD_NAME => 'testUser',
            Comment::FIELD_MESSAGE => 'new test message',
            Comment::FIELD_POST_ID => 1,
        ];
        $comment = new Comment($fields);
        $this->assertInstanceOf('\Models\Post', $comment);
        $this->assertInstanceOf('\Models\Comment', $comment);

        $this->assertEquals($fields[Comment::FIELD_EMAIL], $comment->getEmail());
        $this->assertEquals($fields[Comment::FIELD_NAME], $comment->getName());
        $this->assertEquals($fields[Comment::FIELD_MESSAGE], $comment->getMessage());
        $this->assertEquals($fields[Comment::FIELD_POST_ID], $comment->getPostId());
    }

    /**
     * @test
     * @expectedException \Models\Exceptions\AppException
     * @expectedExceptionCode 400
     * @expectedExceptionMessage Bad request
     */
    public function constructWithoutPostId()
    {
        $fields = [
            Comment::FIELD_EMAIL => 'test@localhost.com',
            Comment::FIELD_NAME => 'testUser',
            Comment::FIELD_MESSAGE => 'new test message',
        ];
        new Comment($fields);
    }

    /**
     * @test
     * @expectedException \Models\Exceptions\AppException
     * @expectedExceptionCode 400
     * @expectedExceptionMessage Bad request
     */
    public function constructEmptyPostId()
    {
        $fields = [
            Comment::FIELD_EMAIL => 'test@localhost.com',
            Comment::FIELD_NAME => 'testUser',
            Comment::FIELD_MESSAGE => 'new test message',
            Comment::FIELD_POST_ID => '',
        ];
        new Comment($fields);
    }

    /**
     * @test
     * @expectedException \Models\Exceptions\AppException
     * @expectedExceptionCode 400
     * @expectedExceptionMessage Bad request
     */
    public function constructZeroPostId()
    {
        $fields = [
            Comment::FIELD_EMAIL => 'test@localhost.com',
            Comment::FIELD_NAME => 'testUser',
            Comment::FIELD_MESSAGE => 'new test message',
            Comment::FIELD_POST_ID => 0,
        ];
        new Comment($fields);
    }

    /**
     * @test
     * @expectedException \Models\Exceptions\AppException
     * @expectedExceptionCode 400
     * @expectedExceptionMessage Bad request
     */
    public function constructZeroStringPostId()
    {
        $fields = [
            Comment::FIELD_EMAIL => 'test@localhost.com',
            Comment::FIELD_NAME => 'testUser',
            Comment::FIELD_MESSAGE => 'new test message',
            Comment::FIELD_POST_ID => '0',
        ];
        new Comment($fields);
    }
}

<?php
use Models\Post;

class PostTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function constructAllFields()
    {
        $fields = [
            Post::FIELD_EMAIL => 'test@localhost.com',
            Post::FIELD_NAME => 'testUser',
            Post::FIELD_MESSAGE => 'new test message'
        ];

        $post = new Post($fields);

        $this->assertEquals($fields['email'], $post->getEmail());
        $this->assertEquals($fields['name'], $post->getName());
        $this->assertEquals($fields['message'], $post->getMessage());
    }

    /**
     * @test
     * @expectedException \Models\Exceptions\AppException
     * @expectedExceptionCode 400
     * @expectedExceptionMessage Bad request
     */
    public function constructEmptyFields()
    {
        new Post([]);
    }

    /**
     * @test
     * @expectedException \Models\Exceptions\AppException
     * @expectedExceptionCode 400
     * @expectedExceptionMessage Bad request
     */
    public function constructWithoutEmail()
    {
        $fields = [
            Post::FIELD_NAME => 'testUser',
            Post::FIELD_MESSAGE => 'new test message'
        ];
        new Post($fields);
    }

    /**
     * @test
     * @expectedException \Models\Exceptions\AppException
     * @expectedExceptionCode 400
     * @expectedExceptionMessage Bad request
     */
    public function constructWithoutName()
    {
        $fields = [
            Post::FIELD_EMAIL => 'test@localhost.com',
            Post::FIELD_MESSAGE => 'new test message'
        ];

        new Post($fields);
    }

    /**
     * @test
     * @expectedException \Models\Exceptions\AppException
     * @expectedExceptionCode 400
     * @expectedExceptionMessage Bad request
     */
    public function constructWithoutMessage()
    {
        $fields = [
            Post::FIELD_EMAIL => 'test@localhost.com',
            Post::FIELD_NAME => 'testUser',
        ];

        new Post($fields);
    }
}


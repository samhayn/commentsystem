/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var SERVER_URL = 'http://localhost/blog-server';
var Controller = function()
{
    this.init = function()
    {
        var self = this;
        this.showNewPostForm();
        $('.form-create-post').on('submit', function(){
            var postForm = $(this);
            var post = {
                'name': $('#name', postForm).val(),
                'email': $('#email', postForm).val(),
                'message': $('#message', postForm).val()
            };

            self.cleanPostForm();

            self.createPost(post);
        });
        $('#post-list').on('click', function(){
            self.init();
        });
        this.getPosts();
    };

    this.cleanPostForm = function()
    {
        $('input, textarea', '.form-create-post').each(function(){
            $(this).val('');
        });
    };

    this.showNewPostForm = function()
    {
        var html = '<form class="form-create-post">' +
                '<h2 class="form-create-post-heading">Type new post!</h2>' +
                '<input type="text" class="input-block-level" id="name" placeholder="Name">' +
                '<input type="text" class="input-block-level" id="email" placeholder="Email">' +
                '<textarea class="form-control" id="message" rows="3"></textarea>' +
                '<button class="btn btn-large btn-primary" type="submit">New post!</button>' +
            '</form>';
        $('.form-container').html(html);
    };

    this.createPost = function(post)
    {
        $.ajax(
            {
                url: SERVER_URL + '/posts',
                type: 'POST',
                dataType: 'JSON',
                async:false,
                data: JSON.stringify(post)
            }
        ).done(this.getPosts());
    };

    this.getPosts = function()
    {
        var self = this;
        $.ajax(
            {
                url: SERVER_URL + '/posts',
                type: 'GET'
            }
        ).done(
            function(data)
            {
                self.PostCollection = new Collection();

                for (var i = 0; i < data.count; i++)
                {
                    var description = data.posts[i].message.split(' ', 2);
                    var Post = {
                        'id' : data.posts[i].id,
                        'name': data.posts[i].name,
                        'email': data.posts[i].email,
                        'message': data.posts[i].message,
                        'description': description
                    };
                    self.PostCollection.push(Post);
                }

                self.render();
            }
        );
    };

    this.render = function()
    {
        var htmlRow = '';
        for (var i = 0; i < this.PostCollection.count(); i++){
            var Post = this.PostCollection.getElementByIndex(i);
            if (Post !== null)
            {
                htmlRow += this.formatPost(Post, true);
            }
        }
        $('.blog-main').html(htmlRow);

        var self = this;

        $('.view-details').on('click', function(e,d){
            self.viewDetails($(this).data('id'));
        });
    };

    this.formatPost = function(Post, showDetailsButton)
    {
        var detailsButton = showDetailsButton ?
            '<button class="btn view-details" data-id="' + Post.id + '"> View details </button>' :
            '';

        var text = showDetailsButton ? Post.description : Post.message;

        return '<div class="blog-post">' +
            '<p class="blog-post-meta">Post written by <a href="#">' +
            Post.name + '</a><a href="mailto:' + Post.email + '" class="email">email:' +
            Post.email + '</a></p>' +
        '<p>' + text + '</p>' +
        detailsButton +
        '</div>';
    };

    this.formatComment = function(Comment)
    {
        return '<div class="blog-post">' +
            '<p class="blog-post-meta">Comment written by <a href="#">' +
            Comment.name +
            '</a><a href="mailto:' + Comment.email +
            '" class="email">email:' + Comment.email + '</a></p>' +
        '<p>' + Comment.message + '</p>' +
        '</div>';
    };

    this.viewDetails = function(id)
    {
        $('.form-container').html('');
        this.getComments(id);
    };

    this.getComments = function(id)
    {
        var self = this;
        $.ajax(
            {
                url: SERVER_URL + '/posts/' + id + '/comment',
                type: 'GET'
            }
        ).done(
            function(data)
            {
                self.CommentCollection = new Collection();

                for (var i = 0; i < data.count; i++)
                {
                    var Comment = {
                        'id' : data.comments[i].id,
                        'name': data.comments[i].name,
                        'email': data.comments[i].email,
                        'message': data.comments[i].message,
                        'postId': data.comments[i].post_id
                    };
                    self.CommentCollection.push(Comment);
                }

                self.renderDetails(id);
            }
        );
    };

    this.renderDetails = function(id)
    {
        var postRow = this.formatPost(this.PostCollection.getElementById(id), false);
        var commentRow = '<div class="container" id="form-comment">' +
                '<h2 class="form-comment-heading">Comment this post</h2>' +
                '<input type="text" class="input-block-level" id="name" placeholder="name">' +
                '<input type="text" class="input-block-level" id="email" placeholder="email">' +
                '<input type="text" class="input-block-level" id="message" placeholder="message">' +
                '<button class="btn btn-large btn-primary" id="submit-button" data-id="' + id + '">Comment!</button>'
                +'</div>';

        if (this.CommentCollection.count() == 0)
        {
            commentRow += '<div class="row"><p>There is no comments for this post yet</p></div>';
        }
        for (var i = 0; i < this.CommentCollection.count(); i++)
        {
            commentRow += this.formatComment(this.CommentCollection.getElementByIndex(i));
        }

        $('.blog-main').html(postRow + commentRow);

        var self = this;

        $('#submit-button').on('click', function(){
            var form = $('#form-comment');

            var comment = {
                'post_id' : id,
                'name': $("#name", form).val(),
                'email': $("#email", form).val(),
                'message': $("#message", form).val()
            };

            self.createComment(comment);
        });
    };

    this.createComment = function(comment)
    {
        $.ajax(
            {
                url: SERVER_URL + '/posts/comment',
                type: 'POST',
                dataType: 'JSON',
                data: JSON.stringify(comment),
                async:false
            }
        ).done(
            this.getComments(comment.post_id)
        );
    };
};
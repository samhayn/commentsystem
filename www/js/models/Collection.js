var Collection = function() {
    var collection = [];
    this.push = function(entity)
    {
        return collection.push(entity);
    };
    this.getElementById = function(id)
    {
        for (var i = 0;i < this.count(); i++ )
        {
            if (collection[i].id == id)
            {
                return collection[i];
            }
        }

        return null;
    };
    this.getCollection = function()
    {
        return collection;
    };
    this.count = function()
    {
        return collection.length;
    };
    this.getElementByIndex = function(index)
    {
        if (collection[index])
        {
            return collection[index];
        }

        return null;
    };
};